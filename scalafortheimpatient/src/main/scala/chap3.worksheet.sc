import scala.collection.mutable.ArrayBuffer
/*
* Things learnt
* yield
* until is better than n-1
* javaconversions
* ArrayBuffer has appendAll
* dropRight is complementary of drop
*/
import util.Random
print("Chapter 3")
val rnd = new Random(42)
val n = 5
val a = (for( i <- 1 to n ) yield rnd.nextInt % n).toArray 

//3.2
val a2 = Array(1,2,3,4,5)
def swapAdj(a: Array[Int]): Unit = {
	for(i <- 2 to a.length by 2){
		var temp = a(i-1)
		a(i-1) = a(i-2)
		a(i-2) = temp
	}
}
swapAdj(a2)
a2.mkString(",")

//3.3
val a3 = for(i <- 0 until a2.length) yield {
	if (a2.length-1 == i) a2(i) else if (i % 2 == 0) a2(i+1) else a2(i-1)
}

//3.4
val a4 = Array(1,2,0,2,-9,-8,8,1,0,6) 
val posIndex = for(i <- 0 until a4.length if a4(i) > -1) yield i 
var a4res = for(i <- 0 until a4.length if (posIndex contains i)) yield a4(i)
a4res = a4res ++ (for (i<- 0 until a4.length if !(posIndex contains i)) yield a4(i))
a4res 

//3.5
def avg(a: Array[Double]) : Double = a.sum / a.length
val a5 = Array(1.2d,1.4d)
avg(a5)

//3.6
val a6 = ArrayBuffer(1,2,0,2,-9,-8,8,1,0,6) 
a6.sorted.mkString(",")
a6.sorted.reverse

//3.7
a6.distinct.mkString(",")

//3.8
val a7 = ArrayBuffer(1,2,0,2,-9,-8,8,1,0,-6) 
var indexes = for(i <- 0 until a7.length if a7(i) < 0) yield i 
var remIndexes = indexes.reverse.dropRight(1)
for(i <- remIndexes) {
	a7.remove(i)
}
a7 

//3.9
val timezones = java.util.TimeZone.getAvailableIDs()
val v = for(tz <- timezones if tz.startsWith("America/")) yield tz.split("/").tail.mkString
v.mkString(",")

//3.10
import java.awt.datatransfer._
val flavors = SystemFlavorMap.getDefaultFlavorMap().asInstanceOf[SystemFlavorMap]
flavors.getNativesForFlavor(DataFlavor.imageFlavor)