// 6.1
object Conversions {
	def inchesToCentimeters(inch: Double) = 2.54*inch
	def gallonsToLiters(gallon: Double) = 3.78541*gallon
	def milesToKilometers(miles: Double) = 1.6*miles
}
Conversions.inchesToCentimeters(1)

//6.2
abstract class UnitConversions {
	def convert(fromUnit: Double): Double
}
object InchesToCentimeters extends  UnitConversions{
	def convert(fromUnit: Double): Double = 2.54*fromUnit 
}
object GallonsToLiters{
	def convert(fromUnit: Double): Double = 3.78541*fromUnit
}
object MilesToKilometers{
 def convert(fromUnit: Double): Double = 1.6*fromUnit
}

InchesToCentimeters.convert(1)

// Not a good idea because Point class has methods to move the point but origin 
// should not be moveable

class Point(val x:Double,val y:Double ){
}
object Point{
	def apply(x: Double, y:Double) = new Point(x,y)
}

val p = Point(3,4)

object CardSuits extends Enumeration {
	type CardSuits = Value
	val Spade = Value("\u2660")
	val Club = Value("\u2663")
	val Heart = Value("\u2665")
	val Diamond = Value("\u2666")
}
def isRed(suit: CardSuits.CardSuits) = (suit == CardSuits.Heart) || (suit == CardSuits.Diamond)
for (s <- CardSuits.values) println(s"${s}")
for (s <- CardSuits.values) println(s"${s} isRed=${isRed(s)}")