def signum(n: Int): Int = if (n > 0) 1 else if (n < 0) -1 else 0
var v1 = signum(4)
//val x = v1 = 3
for(i <- 10 to 1 by -1){
	print(i)
}
def countdown(n: Int): Unit = {
	for(i <- n to 1 by -1){
		print(i+" ")
	}
}
countdown(5)
def toUnicode(n: String): Long  = {
	var p = 1L
	for(c <- n){
		p = p*c.toInt
		// Long Short Int all lead to same value
	}
	p	
}
toUnicode("Hello")
"Hello".foldLeft(1L)((a,b) => a*b.toLong)
// foldLeft can have any type but not reduce ? 
def product(s: String): Long = s.foldLeft(1L)(_*_.toLong)
product("Hello")
def product_r(s: String): Long = if (s.length == 0) 1L else s.head.toLong*product_r(s.tail)
product_r("Hello")
def compute(x:Double,n:Int):Double = if (n==0) 1
	else if(n % 2 == 0) {val v = compute(x,n/2); v*v}
	else if(n<0) 1.0/compute(x,-1*n)
	else x*compute(x,n-1)
compute(2.0,-2) 