import scala.collection.mutable.LinkedHashMap
import java.util.TreeMap
import java.nio.file.Paths
import java.util.Scanner
//4.1
val gizmoPrices = Map("Gizmo1" -> 10, "Gizmo2" -> 20, "Gizmo3" -> 200)
(for((k,v) <-  gizmoPrices) yield (k,v*0.9)).toMap

//4.2
val cwd = Paths.get(".").toAbsolutePath.toString().dropRight(1)
val in = new Scanner(new java.io.File(cwd+"filecount.txt"))
val words = "\\W+"
val wordCount = scala.collection.mutable.Map[String,Int]().withDefaultValue(0)
while(in.hasNext()){
	var l = in.next()
	for( w <- l.split(words) )  wordCount(w) += 1
}
in.close()
wordCount
//4.3

val in2 = new Scanner(new java.io.File(cwd+"filecount.txt"))
var wordCountImmutable = scala.collection.immutable.Map[String,Int]().withDefaultValue(0)
while(in2.hasNext()){
	var l = in2.next()
	for( w <- l.split(words) )  {
		wordCountImmutable += (w -> (wordCountImmutable(w) + 1))
	}
}
wordCountImmutable

//4.4
val in3 = new Scanner(new java.io.File(cwd+"filecount.txt"))
var wordCountSorted = scala.collection.immutable.SortedMap[String,Int]().withDefaultValue(0)
while(in3.hasNext()){
	var l = in3.next()
	for( w <- l.split(words))  {
		wordCountSorted += (w -> (wordCountSorted(w) + 1))
	}
}
wordCountSorted
wordCountSorted.keySet.tail.head

//4.5
import scala.collection.JavaConverters.mapAsScalaMap
val in4 = new java.util.Scanner(new java.io.File(cwd+"filecount.txt"))
var wordCountSorted2  = mapAsScalaMap(new TreeMap[String,Int]()).withDefaultValue(0)

while(in4.hasNext()){
	var l = in4.next()
	for( w <- l.split(words))  {
		wordCountSorted2(w) += 1
	}
}
wordCountSorted2

//4.6
var days = LinkedHashMap[String,Int]()
days += (
	"Monday" -> java.util.Calendar.MONDAY,
	"Tueday" -> java.util.Calendar.TUESDAY,
	"Wednesday" -> java.util.Calendar.WEDNESDAY,
	"Thursday" -> java.util.Calendar.THURSDAY,
	"Friday" -> java.util.Calendar.FRIDAY,
	"Saturday" -> java.util.Calendar.SATURDAY,
	"Sunday" -> java.util.Calendar.SUNDAY,
)
days.keySet.toList

//4.7
import scala.collection.JavaConverters.propertiesAsScalaMap
val props = propertiesAsScalaMap(System.getProperties())
for((k,v) <- props) println(k +"\t|" + v) 

//4.8
val l = Array(1,3,4,9,0)

def minmax(a : Array[Int]):(Int,Int) = {
	var min = a(0)
	var max = a(0)
	for(i <- l){
		if( i < min) min = i	
		if( i > max ) max = i	
	}
	(min,max)
}
minmax(l)

//4.9
def lteqgt(values: Array[Int], v:Int) = {
	var (lt,eq,gt) = (0,0,0)
	for(n <- values){
		 if(n < v)  
		 	lt = lt+1
		else if(n > v) 
			gt  = gt+1 
		else 
			eq=eq+1
	}
	(lt,eq,gt)
}
lteqgt(l,2)
lteqgt(l,3)

//4.10
"Hello".zip("World")