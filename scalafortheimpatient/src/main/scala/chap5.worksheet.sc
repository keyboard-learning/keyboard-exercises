// 5.1
class Counter{
	private var value = 0
	def increment() =  if (value < Int.MaxValue) value += 1
	def current = value
}

// 5.2
class BankAccout{
	private var balance = 0
	def deposit(amt: Int) = balance += amt
	def withdraw(amt: Int) = balance -= amt
	def currentBalance = balance
}

// 5.3

class Time(val hours:Int , val minutes:Int){
		def before(other: Time) : Boolean = (hours < other.hours) || (minutes < other.minutes)
}

val t1 = new Time(4,5)
val t2 = new Time(4,6)
t1.before(t2)
t2.before(t1)

//5.4
class Time2(val hours:Int,val minutes: Int){
	val minSinceMidnight = hours*60 + minutes
	def before(other: Time2) : Boolean = minSinceMidnight < other.minSinceMidnight
}

//5.5 StudentChap5.scala in same folder

//5.6
object person{
	class Person(var age: Int){
		if (age < 0) age = 0 
	}
	var p = new Person(-9)
	p.age
} 
//5.7
class Person1(var name: String){
	val firstName = name.split(" ")(0)
	val lastName = name.split(" ")(1)
}

var p1 = new Person1("Fred Smith")
p1.firstName
p1.lastName

//5.8
class Car{
	private var manufacturer_ = "" 
	private var modelName_ = ""
	private var modelYear_ = -1 
	private var licensePlate_ = ""
	
	def this(manufacturer:String, modelName:String) = {
		this()
		this.manufacturer_ = manufacturer
		this.modelName_ = modelName
	} 
	
	def this(manufacturer:String, modelName:String,modelYear:Int) = {
		this(manufacturer,modelName)
		this.modelYear_ = modelYear
	}
	def this(manufacturer:String, modelName:String, licensePlate:String) = {
		this(manufacturer,modelName)
		this.licensePlate_ = licensePlate
	}
	
	def this(manufacturer:String, modelName:String,modelYear:Int, licensePlate:String) = {
		this(manufacturer,modelName)
		this.licensePlate_ = licensePlate
		this.modelYear_ = modelYear
	}
	def manufacturer = manufacturer_ 
	def modelName = modelName_ 
	def modelYear = modelYear_ 
	def licensePlate = licensePlate_ 
}

var c = new Car("Ford","Jeep")
c.manufacturer

//3.10
class Employee(val name:String="John Q. Public",var salary:Double=0.0){
}
var e: Employee = new Employee